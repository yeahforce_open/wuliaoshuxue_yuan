export class Constants {
  static readonly INDEX_ROUTE = 'pages/Index';
  static readonly AUTH_ROUTE = 'pages/Auth';
  static readonly FUNCTION_ROUTE = 'pages/CloudFunction';
  static readonly DB_ROUTE = 'pages/CloudDb';
  static readonly STORAGE_ROUTE = 'pages/CloudStorage';
  static readonly LEVEL_ROUTE = 'pages/Levels';
  static readonly PRIVACY_POLICY_ROUTE = 'pages/PrivacyPage';
  static readonly SERVICE_POLICY_ROUTE = 'pages/ServicePolicyPage';

  static readonly APP_NAME = '无聊数学';



  //
  static readonly CUR_QUES_NO = "CUR_QUES_NO";
  static readonly INIT_QUES_NO = 1;
  //
  static readonly MATH_LEVEL = "MATH_LEVEL";
  static readonly NEW_START_QUES_NO = "NEW_START_QUES_NO";


  static readonly IS_AGREE_POLICY = "IS_AGREE_POLICY";


  static readonly YES = 1;
  static readonly NO = 0;
}