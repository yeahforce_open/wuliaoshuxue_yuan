export class Question {
  quesNo: number;
  question: string;
  hasImg:boolean;
  qImg: string;
  qImgURL: string;
  answer: string;
  tip: string;
  tipImg: string;

  constructor(quesNo: number, question: string, hasImg , qImg: string, answer: string,
              tip: string, tipImg: string) {

    this.quesNo = quesNo;
    this.question = question;
    this.hasImg = hasImg;
    this.qImg = qImg;
    this.answer = answer;
    this.tip = tip;
    this.tipImg = tipImg;

  }
}
