import dataPreferences from '@ohos.data.preferences';
import Preferences from '@ohos.data.preferences';
import hilog from '@ohos.hilog';
import { Constants } from '../common/Constants';


const TAG = "[UserDataUtils]";
const STORE_NAME = "wuliaoshuxue_store";

export class UserDataUtils {
  async loadUserData(context) {

    const TAGFUN = "[" + TAG + "->loadUserData]";

    hilog.info(1, TAG, "Start", "");

    try {
      dataPreferences.getPreferences(context, STORE_NAME, function (err, preferences) {
        if (err) {
          console.info("Failed to get preferences. code =" + err.code + ", message =" + err.message);
          return;
        }

        // 进行相关数据操作
        preferences.has(Constants.CUR_QUES_NO, function (err, val) {
          if (err) {
            hilog.error(1, TAGFUN, 'Failed to get preferences. Code:%d,message:%s,', err.code, err.message);
          }
          else if (val) {
            hilog.info(1, TAGFUN, "The key '%s' is contained.", Constants.CUR_QUES_NO);
            preferences.get(Constants.CUR_QUES_NO, Constants.INIT_QUES_NO, (err, val) => {
              if (err) {
                hilog.error(1, TAGFUN, 'Failed to get preferences. Code:%d,message:%s,', err.code, err.message);
                return;
              }
              hilog.info(1, TAGFUN, "Succeeded in getting value of '%s : %d'", Constants.CUR_QUES_NO, val);

              AppStorage.SetOrCreate(Constants.CUR_QUES_NO, val)
            })
          } else {
            hilog.info(1, TAGFUN, "The key '%s' does not contain.", Constants.CUR_QUES_NO);
            // 此处以此键值对不存在时写入数据为例
            preferences.put(Constants.CUR_QUES_NO, Constants.INIT_QUES_NO, (err) => {
              if (err) {
                hilog.error(1, TAGFUN, 'Failed to get preferences. Code:%d,message:%s,', err.code, err.message);
                return;
              }
              //
              AppStorage.SetOrCreate(Constants.CUR_QUES_NO, Constants.INIT_QUES_NO)
              //
              hilog.info(1, TAGFUN, "Succeeded in putting data, key : %s", Constants.CUR_QUES_NO);
            })
          }
        })

        console.info("Succeeded flush user data.");
      })

    } catch (err) {
      console.error(`Failed to get preferences. Code:${err.code},message:${err.message}`);
    }

  }

  async flushUserData(context) {

    const TAGFUN = "[" + TAG + "->flushUserData]";

    hilog.info(1, TAGFUN, "Start", "");

    try {

       dataPreferences.getPreferences(context, STORE_NAME, function (err, preferences) {
         if (err) {
           console.info("Failed to get preferences. code =" + err.code + ", message =" + err.message);
           return;
         }
         ///////
         let currentQuestionNo: number = AppStorage.Get<number>(Constants.CUR_QUES_NO);
         hilog.info(1, TAGFUN, 'get currentQuestionNo : %d', currentQuestionNo);
         preferences.put(Constants.CUR_QUES_NO, currentQuestionNo, (err) => {
           if (err) {
             hilog.error(1, TAGFUN, 'Failed to get preferences. Code:%d,message:%s,', err.code, err.message);
           }
           hilog.info(1, TAGFUN, 'Succeeded in putting currentQuestionNo. ');
         })
         ///////
         let isAgreePolicy: number = AppStorage.Get<number>(Constants.IS_AGREE_POLICY);
         hilog.info(1, TAGFUN, 'get isAgreePolicy : %d', isAgreePolicy);
         preferences.put(Constants.IS_AGREE_POLICY, isAgreePolicy, (err) => {
           if (err) {
             hilog.error(1, TAGFUN, 'Failed to get preferences. Code:%d,message:%s,', err.code, err.message);
           }
           hilog.info(1, TAGFUN, 'Succeeded in putting isAgreePolicy. isAgreePolicy: %d', isAgreePolicy);
         })
         ///////
         preferences.flush((err) => {
           if (err) {
             hilog.error(1, TAGFUN, 'Failed to get preferences. Code:%d,message:%s,', err.code, err.message);
             return;
           }
           console.info('Succeeded in flushing.');

           hilog.info(1, TAGFUN, 'Succeeded in getting preferences.');
         })


       })

    } catch (err) {
      hilog.error(1, TAGFUN, 'Failed to get preferences. Code:%d,message:%s,', err.code, err.message);
    }

  }


  async readAgreePolicy(context) {
    const TAGFUN = "[" + TAG + "->readAgreePolicy]";
    hilog.info(1, TAGFUN, "Start", "");

    try {
        dataPreferences.getPreferences(context, STORE_NAME, function (err, preferences) {
          if (err) {
            console.info("Failed to get preferences. code =" + err.code + ", message =" + err.message);
            return;
          }

          // 进行相关数据操作
          preferences.has(Constants.IS_AGREE_POLICY, function (err, valHas) {
            if (err) {
              hilog.error(1, TAGFUN, 'Failed to get preferences. Code:%d,message:%s,', err.code, err.message);
            }

            if (valHas) {
              hilog.info(1, TAGFUN, "The key '%s' is contained.", Constants.IS_AGREE_POLICY);
              preferences.get(Constants.IS_AGREE_POLICY, Constants.NO, (err, valGet) => {
                if (err) {
                  hilog.error(1, TAGFUN, 'Failed to get preferences. Code:%d,message:%s,', err.code, err.message);
                  return;
                }
                hilog.info(1, TAGFUN, "Succeeded in getting value of '%s : %s'", Constants.IS_AGREE_POLICY, valGet);

                AppStorage.SetOrCreate(Constants.IS_AGREE_POLICY, valGet)
              })
            } else {
              hilog.info(1, TAGFUN, "The key '%s' does not contain.", Constants.IS_AGREE_POLICY);

              AppStorage.SetOrCreate(Constants.IS_AGREE_POLICY, Constants.NO)
              // 此处以此键值对不存在时写入数据为例
              preferences.put(Constants.IS_AGREE_POLICY, Constants.NO, (err) => {
                if (err) {
                  hilog.error(1, TAGFUN, 'Failed to get preferences. Code:%d,message:%s,', err.code, err.message);
                  return;
                }
                hilog.info(1, TAGFUN, "Succeeded in putting data, key : %s", Constants.IS_AGREE_POLICY);
              })

            }
          })

        })


    } catch (err) {
      hilog.error(1, TAGFUN, 'Failed to get preferences. Code:%d,message:%s,', err.code, err.message);
    }

  }
}

