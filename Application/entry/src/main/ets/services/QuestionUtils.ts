import hilog from '@ohos.hilog';
import buffer from '@ohos.buffer';
import { LevelDescription } from './LevelDescription';
import { Level } from '@ohos/hypium/src/main/Constant';
import { MathLevel } from './MathLevel';
import { Question } from './Question';
import { Constants } from '../common/Constants';

const TAG = "[QuestionUtils]";

const questionJsonFile = "questions.json";


export async function readLevels(context) {

  let questionTotal: number = 0;
  let qImgRoot: string = null;
  let levels: Array<LevelDescription> = [];
  let questions: Array<Question> = [];

  try {
    context.resourceManager.getRawFileContent(questionJsonFile, async (error, value) => {
      if (error != null) {
        console.log("getLevels error is " + error);
      } else {

        let buf = buffer.from(value);
        let jString = buf.toString();

        hilog.info(1, "getLevels jString ", jString);
        let objLvl = JSON.parse(jString);

        questionTotal = objLvl['questionTotal'];
        qImgRoot = objLvl['qImgRoot'];

        hilog.info(1, "getLevels questionTotal ", "%d", questionTotal);
        hilog.info(1, "getLevels qImgRoot ", qImgRoot);
        hilog.info(1, "getLevels levels length ", "%d", objLvl['levels'].length);

        //Level
        let index: number = 0;
        let length: number = objLvl['levels'].length;
        for (index = 0; index < length; index++) {
          let levelNo: string = objLvl['levels'][index]['levelNo'];
          let levelTitle: string = objLvl['levels'][index]['levelTitle'];
          let mathLevels: Array<number> = objLvl['levels'][index]['questionNos'];
          //
          hilog.info(1, "getLevels mathLevels length ", "%d", mathLevels.length);
          //
          levels.push(new LevelDescription(levelNo, levelTitle, mathLevels));
        }

        //question
        index = 0;
        length = objLvl['questions'].length;
        for (index = 0; index < length; index++) {
          let quesNo: number = objLvl['questions'][index]['quesNo'];
          let question: string = objLvl['questions'][index]['question'];
          let qImg: string = objLvl['questions'][index]['qImg'];
          let hasImg: boolean = true;
          let answer: string = objLvl['questions'][index]['answer'];
          let tip: string = objLvl['questions'][index]['tip'];
          let tipImg: string = objLvl['questions'][index]['tipImg'];
          //
          if(qImg == null || qImg == undefined || qImg == ''){
            hasImg = false;
          }

          //
          let oQuestion: Question = new Question(quesNo, question,hasImg, qImg, answer, tip, tipImg);
          questions.push(oQuestion);
        }

        let oMathLevel: MathLevel = new MathLevel(questionTotal, qImgRoot, levels, questions);

        AppStorage.SetOrCreate<MathLevel>(Constants.MATH_LEVEL, oMathLevel);

        let mathLevel: MathLevel = AppStorage.Get<MathLevel>(Constants.MATH_LEVEL);
        if( mathLevel == null || mathLevel == undefined){
          hilog.error(1, TAG, "%s", "test->mathLevel is null.")
          hilog.error(1, TAG, '%s', 'load math levels failed')
          return;
        }

        hilog.info(1, TAG, '%s', 'load math levels ok')
      }
    });

  } catch (error) {
    console.error(`callback getRawFileContent failed, error code: ${error.code}, message: ${error.message}.`)
  }
}