import { LevelDescription } from './LevelDescription';
import { Question } from './Question';

export class MathLevel {
  questionTotal: number;
  qImgRoot: string;
  levels: Array<LevelDescription>;
  questions: Array<Question>;

  constructor(questionTotal: number, qImgRoot: string, levels: Array<LevelDescription>, questions: Array<Question>) {
    this.questionTotal = questionTotal;
    this.qImgRoot = qImgRoot;
    this.levels = levels;
    this.questions = questions;
  }
}