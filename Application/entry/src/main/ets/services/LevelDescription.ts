export class LevelDescription {
  levelNo : string;
  levelTitle : string;
  questionNos : Array<number>;


  constructor(levelNo : string,levelTitle : string,questionNos : Array<number> ) {
    this.levelNo = levelNo;
    this.levelTitle = levelTitle;
    this.questionNos = questionNos;
  }

}