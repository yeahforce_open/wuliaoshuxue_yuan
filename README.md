# wuliaoshuxue_yuan

#### 介绍
用数学打发无聊时间，越打发，越灵光。
在任何你觉得无聊的时间，都适合打开无聊数学---莫名等待、有序排队、一路坐车、静默蹲坑，甚至深夜失眠，都可以开启思维烧脑模式。
开始你的挑战吧，看你能闯到哪一关？  
![《无聊数学》海报](https://wuliaoshuxue-1257367486.cos.ap-shanghai.myqcloud.com/opensource/gitee/img/wuliaoshuxue-poster.jpg)  

---
这个为《无聊数学》的鸿蒙系统元服务的源代码，采用ArkTS开发，兼容SDK3.1.0(API9.0)。 

#### 软件架构
本软件采用
关于《无聊数学》的软件架构细节，可以参考华为开发联盟论坛的这篇文章。该文章做了较为详细的介绍。    
[#端云一体化#SHOW出您的元服务#无聊数学](https://developer.huawei.com/consumer/cn/forum/topic/0203131124160856024?fid=0101591351254000314)  



#### 安装教程
这是使用华为开发工具DevEco Studio，创建的端云一体化的工程，使用DevEco Studio导入打开即可。  
`git clone https://gitee.com/yeahforce_open/wuliaoshuxue_yuan.git`


#### 使用说明
鸿蒙系统元服务（原名为原子化服务）是HarmonyOS提供的一种面向未来的服务提供方式，是有独立入口、免安装、可为用户提供一个或多个便捷服务的新型应用程序形态。



